# frozen_string_literal: true

# Helpers for working with the slack responses
module SlackHelper
  # Returns a slack markdown block
  #
  # text  - Block content
  def slack_mrkdwn_block(text:, context_elements: [])
    block = [
      {
        type: 'section',
        text: {
          type: 'mrkdwn',
          text: text
        }
      }
    ]

    if context_elements.empty?
      block
    else
      context = {
        type: 'context',
        elements: context_elements
      }

      block << context
    end
  end
end

RSpec.configure do |config|
  config.include SlackHelper
end
