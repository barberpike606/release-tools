# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Deployments::DeploymentVersionParser do
  describe '#parse' do
    context 'when parsing an invalid version' do
      it 'raises ArgumentError' do
        expect { described_class.new.parse('foo') }
          .to raise_error(ArgumentError)
      end
    end

    context 'when parsing an auto-deploy version with release metadata' do
      it 'returns a version for the auto-deploy branch' do
        pkg_version = '12.7.202001101501-94b8fd8d152.6fea3031ec9'
        product_version = ReleaseTools::ProductVersion.from_auto_deploy(pkg_version)
        allow(product_version).to receive(:metadata).and_return(
          {
            'releases' => {
              'gitlab-ee' => {
                'ref' => '1-2-auto-deploy-12345678',
                'sha' => '94b8fd8d152680445ec14241f14d1e4c04b0b5ab'
              }
            }
          }
        )

        expect(ReleaseTools::ProductVersion)
          .to receive(:from_auto_deploy)
          .with(pkg_version)
          .and_return(product_version)

        version =
          described_class.new.parse(pkg_version)

        expect(version.ref).to eq('1-2-auto-deploy-12345678')
        expect(version.sha).to eq('94b8fd8d152680445ec14241f14d1e4c04b0b5ab')
        expect(version.tag?).to eq(false)
      end
    end

    context 'when parsing an RC tag' do
      it 'returns a DeploymentVersion' do
        allow(ReleaseTools::GitlabClient)
          .to receive(:tag)
          .and_return(
            double(
              :tag,
              commit: double(
                :commit,
                id: '6614791fadf7a479aea05dada8488d1f64bdb43d'
              ),
              name: 'v12.5.0-rc43-ee'
            )
          )

        version = described_class.new.parse('12.5.0-rc43.ee.0')

        expect(version.ref).to eq('v12.5.0-rc43-ee')
        expect(version.sha).to eq('6614791fadf7a479aea05dada8488d1f64bdb43d')
        expect(version.tag?).to eq(true)
      end
    end

    context 'when parsing a stable tag' do
      it 'returns a DeploymentVersion' do
        allow(ReleaseTools::GitlabClient)
          .to receive(:tag)
          .and_return(
            double(
              :tag,
              commit: double(
                :commit,
                id: '6614791fadf7a479aea05dada8488d1f64bdb43d'
              ),
              name: 'v12.8.0-ee'
            )
          )

        version = described_class.new.parse('12.8.0-ee.0')

        expect(version.ref).to eq('v12.8.0-ee')
        expect(version.sha).to eq('6614791fadf7a479aea05dada8488d1f64bdb43d')
        expect(version.tag?).to eq(true)
      end
    end

    context 'when parsing a deployed tag that does not exist' do
      it 'raises ArgumentError' do
        allow(ReleaseTools::GitlabClient)
          .to receive(:tag)
          .and_raise(gitlab_error(:NotFound))

        expect { described_class.new.parse('12.5.0-rc43.ee.0') }
          .to raise_error(ArgumentError)
      end
    end
  end
end
