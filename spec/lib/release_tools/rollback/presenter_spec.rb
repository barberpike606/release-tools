# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Rollback::Presenter do
  let(:migration) do
    {
      'new_path' => 'db/migrate/20211030_new_migration.rb'
    }
  end

  let(:post_deploy_migration) do
    {
      'new_path' => 'db/post_migrate/20211030_new_post_deploy_migration.rb'
    }
  end

  def comparison_stub(overrides = {})
    defaults = {
      timeout?: false,
      empty?: false,
      migrations: [],
      post_deploy_migrations: [],
      web_url: 'https://example.com/',
      current_package: attributes_for(:omnibus_deployment).fetch(:ref),
      target_package: attributes_for(:omnibus_deployment).fetch(:ref),
      current_rails_sha: SecureRandom.hex(6)[0, 11]
    }

    instance_double('ReleaseTools::Rollback::Comparison', defaults.merge(overrides))
  end

  describe '#present' do
    it 'includes current and target package names' do
      comparison = comparison_stub(
        current_package: '<current package>',
        target_package: '<target package>'
      )

      instance = described_class.new(comparison)
      present = instance.present

      expect(present).to include("*Current:* `<current package>`\n")
      expect(present).to include("*Target:* `<target package>`\n")
    end

    it 'includes a comparison link' do
      comparison = comparison_stub(web_url: '<compare_url>')

      instance = described_class.new(comparison)

      expect(instance.present).to include("*Comparison:* <compare_url>\n")
    end

    it 'includes a warning for a timeout' do
      comparison = comparison_stub(timeout?: true)

      instance = described_class.new(comparison)

      expect(instance.present).to include(":warning: Comparison timed out\n")
    end

    it 'includes a warning for an empty compare' do
      comparison = comparison_stub(empty?: true)

      instance = described_class.new(comparison)

      expect(instance.present).to include(":warning: Comparison was empty\n")
    end

    it 'includes a count of migrations' do
      comparison = comparison_stub(migrations: [migration])

      instance = described_class.new(comparison)
      lines = instance.present.flatten.collect(&:to_s)

      expect(lines).to include(":new: 1 migrations\n")
      expect(lines).not_to include(a_string_matching("post-deploy"))
    end

    it 'includes a count of post-deploy migrations' do
      comparison = comparison_stub(post_deploy_migrations: [post_deploy_migration])

      instance = described_class.new(comparison)
      lines = instance.present.flatten.collect(&:to_s)

      expect(lines).to include(":new: 1 post-deploy migrations\n")
      expect(lines).not_to include(a_string_matching(/\d+ migrations/))
    end

    it 'includes links to new migrations' do
      comparison = comparison_stub(migrations: [migration], post_deploy_migrations: [post_deploy_migration])

      instance = described_class.new(comparison)
      lines = instance.present.flatten.collect(&:to_s)

      expect(lines).to include(a_string_matching(/Migration: \[`20211030_new_migration.rb`\]\(.+\)/))
      expect(lines).to include(a_string_matching(/Post-deploy: \[`20211030_new_post_deploy_migration.rb`\]\(.+\)/))
    end
  end
end
