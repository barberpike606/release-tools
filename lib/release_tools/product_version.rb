# frozen_string_literal: true

module ReleaseTools
  # ProductVersion is a container for any type of GitLab - The DevOps Platform package.
  # Given a Product version we will be able to fetch its own metadata and find
  # what references to both components and packagers.
  #
  # A ProductVersion is always a normalized version (see: Version#to_normalized_version)
  class ProductVersion
    include ::SemanticLogger::Loggable
    extend Forwardable

    # delegate data accessors
    def_delegators :@version, :major, :minor, :patch, :rc, :to_s
    # delegate predicate functions for detecting the type of version
    def_delegators :@version, :rc?, :monthly?, :patch?

    # Creates a `ProductVersion` from an auto-deploy tag or a package version
    #
    # @param version [String, AutoDeploy::Version] the auto-deploy version
    # @return [ProductVersion, nil] the product version of the given auto-deploy
    #   version or nil if not an auto-deploy version.
    def self.from_auto_deploy(version)
      version = AutoDeploy::Version.new(version) unless version.respond_to?(:to_package)

      from_package_version(version.to_package)
    rescue ArgumentError
      nil
    end

    # Creates a `ProductVersion` from an omnibus package version (i.e. $DEPLOY_VERSION)
    #
    # @param version [String, Version] a valid Omnibus package version
    # @return [ProductVersion, nil] the product version of the given version
    #   or nil if not a valid version.
    def self.from_package_version(version)
      version = Version.new(version) unless version.is_a?(Version)
      return nil unless version.version?

      new(version.to_normalized_version)
    end

    def initialize(version)
      @version = Version.new(version)
    end

    def ==(other)
      @version.to_s == other.to_s
    end

    def metadata
      @metadata ||= fetch_metadata
    end

    # extract the release metadata for the given released_item
    #
    # @param released_item [String, Symbol] the component name
    # @return [ReleaseMetadata::Release, nil] the release information
    #   or nil if the component does not exists
    def [](released_item)
      name = released_item.to_s
      raw = metadata.dig('releases', name)
      return unless raw

      ReleaseMetadata::Release.new(name: name, **raw)
    end

    private

    def metadata_path
      "releases/#{major}/#{@version.to_normalized_version}.json"
    end

    def fetch_metadata
      json = Retriable.with_context(:api) do
        logger.info('Fetching release metadata', version: to_s)

        GitlabOpsClient.file_contents(ReleaseMetadataUploader::PROJECT, metadata_path)
      rescue Gitlab::Error::NotFound
        return {}
      end

      JSON.parse(json)
    end
  end
end
