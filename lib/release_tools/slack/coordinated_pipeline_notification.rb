# frozen_string_literal: true

require 'release_tools/time_util'

module ReleaseTools
  module Slack
    class CoordinatedPipelineNotification
      include ::SemanticLogger::Loggable
      include ReleaseTools::TimeUtil

      STATUS_ICONS = {
        started: ':ci_running:',
        finished: ':ci_passing:',
        failed: ':ci_failing:'
      }.freeze

      ENVIRONMENT_ICONS = {
        'gstg-cny': ':hatching_chick:',
        'gstg': ':building_construction:',
        'gprd-cny': ':canary:',
        'gprd': ':party-tanuki:'
      }.freeze

      # deploy_version  - Deployer package
      # pipeline        - GitLab response pipeline object
      # environment     - gstg-cny, gstg, gprd-cny, gprd
      def initialize(deploy_version:, pipeline:, environment:)
        @deploy_version = deploy_version
        @pipeline = pipeline
        @environment = environment
      end

      def execute
        logger.info('Sending slack notification', deploy_version: deploy_version, environment: environment, deployer_url: deployer_url)

        return if SharedStatus.dry_run?

        ReleaseTools::Slack::Message.post(
          channel: ReleaseTools::Slack::ANNOUNCEMENTS,
          message: fallback_text,
          blocks: slack_block
        )
      end

      private

      attr_reader :deploy_version, :pipeline, :environment

      def deployer_url
        pipeline.web_url
      end

      def fallback_text
        "#{environment} #{deployer_status} #{deploy_version}"
      end

      def slack_block
        [
          {
            type: 'section',
            text: ReleaseTools::Slack::Webhook.mrkdwn(section_block)
          },
          {
            type: 'context',
            elements: context_elements
          }
        ]
      end

      def section_block
        [].tap do |text|
          text << environment_icon
          text << status_icon
          text << "*#{environment}*"
          text << "<#{deployer_url}|#{deployer_status}>"
          text << "`#{deploy_version}`"
        end.join(' ')
      end

      def environment_icon
        ENVIRONMENT_ICONS[environment.to_sym]
      end

      def status_icon
        STATUS_ICONS[deployer_status.to_sym]
      end

      def deployer_status
        case pipeline.status
        when 'success'
          'finished'
        when 'failed', 'canceled'
          'failed'
        else
          'started'
        end
      end

      def context_elements
        [].tap do |elements|
          elements << { type: 'mrkdwn', text: ":clock1: #{current_time.strftime('%Y-%m-%d %H:%M')} UTC" }
          elements << { type: 'mrkdwn', text: ":sentry: #{sentry_link}" }
          elements << { type: 'mrkdwn', text: ":timer_clock: #{wall_duration}" } if finished_or_failed?
        end
      end

      def current_time
        @current_time ||= Time.now.utc
      end

      def sentry_link
        version = ReleaseTools::AutoDeploy::Version.new(deploy_version)

        "<https://sentry.gitlab.net/gitlab/gitlabcom/releases/#{version.rails_sha}/|View Sentry>"
      end

      def wall_duration
        duration(current_time - start_time).first
      end

      def start_time
        Time.parse(pipeline.created_at)
      end

      def finished_or_failed?
        deployer_status == 'finished' || deployer_status == 'failed'
      end
    end
  end
end
