# frozen_string_literal: true

module ReleaseTools
  module Rollback
    # Generates a `Rollback::Comparison` between a current state and a target to
    # which we want to roll back.
    #
    # The `current` and the `target` values can be a package String (e.g.,
    # `14.3.202109151120-97529725116.5a421e7fb6d`), or an environment name
    # (e.g., `gprd`).
    #
    # If given an environment name, we fetch the Deployments for that
    # environment. When an environment is used as the `current` argument, we
    # grab the latest Deployment regardless of status; when used as the `target`
    # argument, we grab the latest successful Deployment that isn't the
    # current one.
    #
    # Examples:
    #
    #   # Comparing two specific packages
    #   CompareService.new(
    #     current: '14.3.202109151620-603b96d4843.7b4f35cc217',
    #     target: '14.3.202109151120-97529725116.5a421e7fb6d'
    #   ).execute
    #
    #   # Comparing current and previous `gprd` deployments
    #   CompareService.new(current: 'gprd', target: 'gprd').execute
    #
    #   # Comparing current `gprd-cny` deployment to current `gprd` deployment
    #   CompareService.new(current: 'gprd-cny', target: 'gprd').execute
    #
    #   # Comparing current `gprd` deployment to a specific version
    #   CompareService
    #     .new(current: 'gprd', target: '14.3.202109151120-97529725116.5a421e7fb6d')
    #     .execute
    class CompareService
      include ::SemanticLogger::Loggable

      # Omnibus deployments have package information, from which we can derive
      # the gitlab-rails SHA
      PROJECT = ReleaseTools::Project::OmnibusGitlab.auto_deploy_path

      ENVIRONMENTS = %w[gstg-cny gstg gprd-cny gprd].freeze

      attr_reader :current, :target

      def initialize(current:, target:)
        @current = from_package(current)
        @target = from_package(target)
      end

      def execute
        @current = current_from_deployment(@current) if environment?(@current)
        @target = target_from_deployment(@target) if environment?(@target)

        logger.info("Generating comparison", current: @current, target: @target)

        validate_version!(@current)
        validate_version!(@target)

        @comparison = Rollback::Comparison
          .new(current: @current, target: @target)
          .execute
      end

      private

      def validate_version!(version)
        return if version.is_a?(AutoDeploy::Version)

        logger.fatal("Invalid version for comparison", version: version)
        raise ArgumentError
      end

      def environment?(name)
        ENVIRONMENTS.include?(name.to_s)
      end

      def from_package(package)
        return package if package.nil?

        # If it doesn't match it may be an environment, which we'll load later
        return package unless AutoDeploy::Version.match?(package)

        AutoDeploy::Version.new(package)
      end

      def current_from_deployment(environment)
        from_package(deployments(environment).first.ref)
      end

      def target_from_deployment(environment)
        target = deployments(environment).detect do |dep|
          dep.ref != @current.to_tag && dep.status == 'success'
        end

        from_package(target&.ref)
      end

      def deployments(environment)
        Retriable.with_context(:api) do
          GitlabClient.deployments(PROJECT, environment)
        end
      end
    end
  end
end
